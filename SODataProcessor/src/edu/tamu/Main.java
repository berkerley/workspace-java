package edu.tamu;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.jdom2.Attribute;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.JDOMException;
import org.jdom2.filter.Filters;
import org.jdom2.input.SAXBuilder;
import org.jdom2.xpath.XPathExpression;
import org.jdom2.xpath.XPathFactory;
import org.xml.sax.InputSource;

public class Main {
	private static String fileName = "Posts-1";
	private static String fileExtention_xml = ".xml";
	private static String fileExtention_csv = ".csv";
	private static String filePath = "E:\\Professional\\Data";
	private static String csvFilePath = "E:\\Professional\\Data\\csv";
	public static void main(String[] args){
		
		filePath = filePath.concat(File.separator).concat(fileName).concat(fileExtention_xml);
		csvFilePath = csvFilePath.concat(File.separator).concat(fileName).concat(fileExtention_csv);
		BufferedWriter csvWriter = null;
		try {
			csvWriter = new BufferedWriter(new FileWriter(csvFilePath));
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		SODataProcessor sodp = new SODataProcessor();
		Document sodoc = sodp.readXml(filePath);
		Element root = sodoc.getRootElement();
		
		//1. traverse all the rows and retrieve the needed attributes
		//1.1 Attributes declaration
		List<Element> rowList = root.getChildren();
		//System.out.println("row size is"+rowList.size());
		Element row = null;
		String id = null;
		String acceptedAnswerId = null;  //for question post only
		String questionTimeString = null;
		String acceptedAnswerTimeString = null;
		long questionTime = 0L;
		long acceptedAnswerTime = 0L;
		long acceptedDateDifference = 0L;
		String score = null;
		String viewCount = null;
		String tags = null;
		String answerCount = null;
		String commentCount = null;
		String favoriteCount = null;
		String ownerUserId = null;
		
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		try {
			csvWriter.write("id,date_type1,date_type2,acceptDateDiff,acceptRank,score,views,tags,answers,comments,favorites,ownerUserId");
			csvWriter.write("\n");
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
		//1.2 iterate the row list
		int rowIndex = 0;
		int rowSize = rowList.size();
		System.out.println("There are overall "+rowSize+" rows.");
		for(;rowIndex < rowSize; rowIndex++){
			System.out.println("Processing post "+rowIndex+". ");
			row = rowList.get(rowIndex);
			
			/******************************* Basic attributes of a question ******************************/
			//only handle questions and trace (accepted) answers whose parent is this question
			//question with an accepted answer is kept
			acceptedAnswerId = row.getAttributeValue("AcceptedAnswerId");
			if(acceptedAnswerId == null || "".equals(acceptedAnswerId)){ 
				continue;
			}
			id = row.getAttributeValue("Id");
			questionTimeString = row.getAttributeValue("CreationDate");
			score = row.getAttributeValue("Score")==null?"NA":row.getAttributeValue("Score");
			viewCount = row.getAttributeValue("ViewCount")==null?"0":row.getAttributeValue("ViewCount");
			tags = row.getAttributeValue("Tags")==null?"NA":row.getAttributeValue("Tags");
			answerCount = row.getAttributeValue("AnswerCount")==null?"0":row.getAttributeValue("AnswerCount");
			commentCount = row.getAttributeValue("CommentCount")==null?"0":row.getAttributeValue("CommentCount");
			favoriteCount = row.getAttributeValue("FavoriteCount")==null?"0":row.getAttributeValue("FavoriteCount");
			ownerUserId = row.getAttributeValue("OwnerUserId")==null?"NA":row.getAttributeValue("OwnerUserId");
			
			/******************************* Accepted Date Difference ******************************/
			//Get the answer list of a question
			List<Element> answerList = getAnswerList(rowList, id, answerCount);
			if(answerList == null || answerList.isEmpty()) {
				System.out.println();
				continue;  //ignore those questions without answers or with answers not in the current rowlist
			}
			//Get the ADD between the creation dates of accepted answer and the question
			int answerSize = answerList.size();
			Element answer = null;
			String answerId = null; 
			boolean find = false;  //To tell if the accepted answer id is found in the current file
			for(int answerIndex = 0;answerIndex < answerSize; answerIndex++){
				answer = answerList.get(answerIndex);
				answerId = answer.getAttributeValue("Id");
				
				if(answerId.equals(acceptedAnswerId)){
					find = true;
					acceptedAnswerTimeString = answer.getAttributeValue("CreationDate");
					try {
						questionTime = formatter.parse(questionTimeString).getTime();
						acceptedAnswerTime = formatter.parse(acceptedAnswerTimeString).getTime();
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					acceptedDateDifference = acceptedAnswerTime - questionTime;
					break;
				}
			}
			if(!find){
				System.out.println();
				continue;  //if the accepted answer is not found in the current file continue without this question
			}
			
			/******************************* Rank of Accepted Answer ******************************/
			//Get the rank of the accepted answer in the answerList in terms of the creationDate
			int rank = 1;  //rank is used to count the rank of the accepted answer
			String creationDate_answer = null;
			for(int answerIndex = 0;answerIndex < answerSize; answerIndex++){
				answer = answerList.get(answerIndex);
				creationDate_answer = answer.getAttributeValue("CreationDate");
				long answerTime = 0L;
				try {
					questionTime = formatter.parse(questionTimeString).getTime();
					answerTime = formatter.parse(creationDate_answer).getTime();
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(acceptedDateDifference > answerTime - questionTime) rank++;
			}
			
			//Write all the answer to csv file for the later R processing
			try {
				//creationDate_postType2 is the accepted answer creation date
				csvWriter.write(id+", "+questionTime+", "+acceptedAnswerTime+", "+acceptedDateDifference+","+rank+","+score+","+viewCount+","+tags+","+answerCount+","+commentCount+","+favoriteCount+","+ownerUserId);
				csvWriter.write("\n");
				if(rowIndex % 1000 == 0)
					csvWriter.flush();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("post "+row.getAttributeValue("Id")+" got written into the result file.");
		}
		try {
			csvWriter.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Done!");
	}
	/**
	 * This method is used to get the answer list of a question
	 * @param rowList
	 * @param id
	 * @param answerCount
	 * @return
	 */
	private static List<Element> getAnswerList(List<Element> rowList, String id, String answerCount) {
		int counter = 0;
		List<Element> answerList = new ArrayList<Element>();
		for(Element element : rowList){
			if(id.equals(element.getAttributeValue("ParentId"))){
				answerList.add(element);
				counter++;
				if(counter == Integer.valueOf(answerCount)) break;
			}
		}
		return answerList;
	}
}	













