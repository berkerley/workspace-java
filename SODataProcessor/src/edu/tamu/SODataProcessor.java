package edu.tamu;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.util.regex.Pattern;

import org.jdom2.Document;
import org.jdom2.JDOMException;
import org.jdom2.input.SAXBuilder;

/**
 * This class is a wrapper of the xml parser.
 * The functions it provides include:
 * 1. Read xml file
 * 2. 
 * @author Benke
 *
 */
public class SODataProcessor {
	private SAXBuilder builder = new SAXBuilder();
	
	
	public Document readXml(String filePath){
		Document jdomDoc = null;
		
		try {
			File xml = new File(filePath);
			String xmlString = normalize(xml);
			if(xmlString == null || "".equals(xmlString)) return null;
			jdomDoc = builder.build(new StringReader(xmlString));
			
		} catch (JDOMException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return jdomDoc;
	}
	public String normalize(File xml) throws IOException{
		StringBuilder xmlStringBuilder = new StringBuilder();
		BufferedReader xmlReader = new BufferedReader(new FileReader(xml));
		String line = null;
		int counter = 0;
		while((line = xmlReader.readLine()) != null){
			if(line.contains("Body")||line.contains("Title")){
				//Eliminate the attributes body and title temporarily
				line = line.replaceAll("\\sBody=\"[^\"]*\"\\s", " ");
				line = line.replaceAll("\\sTitle=\"[^\"]*\"\\s", " ");
				//System.out.println(line);
			}
			xmlStringBuilder.append(line);
			counter++;
		}
		System.out.println(counter+" lines processed.");
		return xmlStringBuilder.toString();
	}
}































