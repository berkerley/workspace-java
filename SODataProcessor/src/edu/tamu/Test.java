package edu.tamu;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Test {
	public static void main(String[] args){
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
		try {
			Date startDate = formatter.parse("2008-07-31T21:42:52.667");
			Date endDate = formatter.parse("2008-07-31T22:17:57.883");
			System.out.println("startDate-->"+startDate.toString());
			System.out.println("endDate-->"+endDate.toString());
			System.out.println("Time interval is "+(endDate.getTime()-startDate.getTime()));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
}
