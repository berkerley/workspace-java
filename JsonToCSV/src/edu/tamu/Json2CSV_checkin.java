package edu.tamu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.lang3.StringEscapeUtils;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class Json2CSV_checkin {
	
	public static final String jsonFilePath = "E:\\data\\json\\yelp_academic_dataset_checkin_sample.json";
	public static final String csvFilePath = "E:\\data\\json\\yelp_academic_dataset_checkin_sample.csv";
	
	public static StringBuilder titleBuilder = new StringBuilder();
	public static String separator = "; ";
	
	public static void main(String[] args){
		
		File csvFile = new File(csvFilePath);
		FileWriter csvWriter = null;
		if(!csvFile.exists()){
			try {
				csvFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			csvWriter = new FileWriter(csvFile);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//1. Prepare a json parser
		try {
			JsonFactory jsonFactory = new JsonFactory();
			JsonParser jsonParser = jsonFactory.createParser(new File(jsonFilePath));
			JsonToken token = jsonParser.nextToken();
			StringBuilder rowData = new StringBuilder();
			StringBuilder titleBuilder = new StringBuilder();
			int counter = 0;  //used to count # of tokens
			int rowCounter = 0;
			boolean buildTitle = true;
			while(token != null){
				String fieldName = jsonParser.getCurrentName();
				//////System.out.println("--------------"+fieldName+"-----------------");
				//The next four string builders are temporarily used to hold the sub objects or arrays
				
				if("checkin_info".equalsIgnoreCase(fieldName)){
					if(buildTitle){
						titleBuilder.append(wrapper(fieldName));
					}
					
					counter++;
					//System.out.print(counter+". "+fieldName+"-->");
					
					StringBuilder objectBuilder = new StringBuilder();
					JsonToken thisToken = jsonParser.nextToken();  //starts with {
					String thisName = thisToken.asString();
					
					//////System.out.println(">>>>>>>"+thisName);
					
					JsonToken nextToken = jsonParser.nextToken();  //starts with FIELD_NAME or }
					String nextName = null;
					if(nextToken == JsonToken.END_OBJECT)
						nextName = nextToken.asString();
					else
						nextName = jsonParser.getValueAsString();  //can be field name or value
					//////System.out.println(">>>>>>>"+nextName);
					
					while(!(thisToken == JsonToken.END_OBJECT 
							&& nextToken == JsonToken.FIELD_NAME 
							&& "type".equalsIgnoreCase(nextName))){
						
						
						objectBuilder.append(thisName).append("_");  
						//objectBuilder.append(nextName);
						thisToken = nextToken;  //shift by one
						if(thisToken == JsonToken.START_OBJECT || thisToken == JsonToken.END_OBJECT)
							thisName = thisToken.asString();
						else
							thisName = jsonParser.getValueAsString();
						
						nextToken = jsonParser.nextToken();  //shift by one
						if(nextToken == JsonToken.START_OBJECT || nextToken == JsonToken.END_OBJECT)
							nextName = nextToken.asString();
						else
							nextName = jsonParser.getValueAsString();
					}
					objectBuilder.append(thisName);
					String objectString = objectBuilder.toString();
					
					//System.out.println(StringEscapeUtils.escapeJava(objectString));
					rowData.append(wrapper(objectString));
					
					token = nextToken;
				}else if("type".equalsIgnoreCase(fieldName) || "business_id".equalsIgnoreCase(fieldName)){
					if(buildTitle){
						titleBuilder.append(separator);
						titleBuilder.append(wrapper(fieldName));
					}
					
					counter++;
					//System.out.print(counter+". "+fieldName+"-->");
					token = jsonParser.nextToken();
					rowData.append(separator);
					rowData.append(wrapper(jsonParser.getValueAsString()));
					//System.out.println(StringEscapeUtils.escapeJava(jsonParser.getValueAsString()));
					token = jsonParser.nextToken();
				}else{
					token = jsonParser.nextToken();
				}
				if(counter == 3){  //means one row is done
					//here you should create a csv file and pour the result row string to the csv file
					if(buildTitle){
						////System.out.println(titleBuilder.toString());
						csvWriter.write(titleBuilder.toString());
						csvWriter.write("\n");
						csvWriter.flush();
						buildTitle = false;
					}
					rowCounter++;
					System.out.println("row "+rowCounter+" data transformed to csv!");
					counter = 0;
					csvWriter.write(StringEscapeUtils.escapeJava(StringEscapeUtils.escapeJava(rowData.toString())));
					csvWriter.write("\n");
					csvWriter.flush();
					rowData.setLength(0);
					rowData = new StringBuilder();
				}
			}
			if(csvWriter != null)
				csvWriter.close();
			System.out.println(csvFilePath+" generated!");
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	//This method is used to wrap the value with quote
	public static String wrapper(String content){
		StringBuilder wrapped = new StringBuilder();
		//return wrapped.append("'").append(content).append("'").toString();
		return content;
	}
}
