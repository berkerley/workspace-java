package edu.tamu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.lang3.StringEscapeUtils;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class Json2CSV_tip {
	
	public static final String jsonFilePath = "E:\\data\\json\\yelp_academic_dataset_tip_sample.json";
	public static final String csvFilePath = "E:\\data\\json\\yelp_academic_dataset_tip_sample.csv";
	
	public static StringBuilder titleBuilder = new StringBuilder();
	public static String separator = "; ";
	
	public static void main(String[] args){
		
		File csvFile = new File(csvFilePath);
		FileWriter csvWriter = null;
		if(!csvFile.exists()){
			try {
				csvFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			csvWriter = new FileWriter(csvFile);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//1. Prepare a json parser
		try {
			JsonFactory jsonFactory = new JsonFactory();
			JsonParser jsonParser = jsonFactory.createParser(new File(jsonFilePath));
			JsonToken token = jsonParser.nextToken();
			StringBuilder rowData = new StringBuilder();
			StringBuilder titleBuilder = new StringBuilder();
			int counter = 0;  //used to count # of tokens
			int rowCounter = 0;
			boolean buildTitle = true;
			while(token != null){
				String fieldName = jsonParser.getCurrentName();
				//////System.out.println("--------------"+fieldName+"-----------------");
				//The next four string builders are temporarily used to hold the sub objects or arrays
				
				if("user_id".equalsIgnoreCase(fieldName) || "text".equalsIgnoreCase(fieldName) 
						|| "business_id".equalsIgnoreCase(fieldName) || "likes".equalsIgnoreCase(fieldName)
						|| "date".equalsIgnoreCase(fieldName) || "type".equalsIgnoreCase(fieldName)){
					if(buildTitle){
						if(!"user_id".equalsIgnoreCase(fieldName))
							titleBuilder.append(separator);
						titleBuilder.append(wrapper(fieldName));
					}
					
					counter++;
					//System.out.print(counter+". "+fieldName+"-->");
					token = jsonParser.nextToken();
					if(!"user_id".equalsIgnoreCase(fieldName))
						rowData.append(separator);
					rowData.append(wrapper(jsonParser.getValueAsString()));
					//System.out.println(StringEscapeUtils.escapeJava(jsonParser.getValueAsString()));
					token = jsonParser.nextToken();
				}else{
					token = jsonParser.nextToken();
				}
				if(counter == 6){  //means one row is done
					//here you should create a csv file and pour the result row string to the csv file
					if(buildTitle){
						////System.out.println(titleBuilder.toString());
						csvWriter.write(titleBuilder.toString());
						csvWriter.write("\n");
						csvWriter.flush();
						buildTitle = false;
					}
					rowCounter++;
					System.out.println("row "+rowCounter+" data transformed to csv!");
					counter = 0;
					csvWriter.write(StringEscapeUtils.escapeJava(StringEscapeUtils.escapeJava(rowData.toString())));
					csvWriter.write("\n");
					csvWriter.flush();
					rowData.setLength(0);
					rowData = new StringBuilder();
				}
			}
			if(csvWriter != null)
				csvWriter.close();
			System.out.println(csvFilePath+" generated!");
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	//This method is used to wrap the value with quote
	public static String wrapper(String content){
		StringBuilder wrapped = new StringBuilder();
		//return wrapped.append("'").append(content).append("'").toString();
		return content;
	}
}
