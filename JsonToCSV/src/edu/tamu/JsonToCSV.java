package edu.tamu;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class JsonToCSV {
	public static final String SPLIT = ",";
	public static void main(String[] args){
		//1. Prepare a json parser
		JsonFactory jsonFactory = new JsonFactory();
		JsonParser jsonParser = null;
		try {
			jsonParser = jsonFactory.createParser(new File("E:\\data\\json\\student.json"));
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//2. Use the parser to crawl the input json file
		try {
			String fieldName = null;
			StringBuilder row = new StringBuilder();
			int counter = 0;
			while(jsonParser.nextToken() != JsonToken.END_ARRAY){
				counter++;
				//fieldName = jsonParser.getCurrentName();
				fieldName = jsonParser.getValueAsString();
				System.out.println(counter+"-->"+fieldName+"\t"+jsonParser.getCurrentToken().asString());
				if("name".equals(fieldName)){
					jsonParser.nextToken();
					row.append(jsonParser.getValueAsString());
					counter++;
					System.out.println(counter+"-->"+jsonParser.getValueAsString());
					
					row.append(SPLIT);
				}
				
				if("age".equals(fieldName)){
					jsonParser.nextToken();
					row.append(jsonParser.getValueAsInt());
					counter++;
					System.out.println(counter+"-->"+jsonParser.getCurrentToken().name());
					System.out.println(row.toString());
					row = new StringBuilder();
				}
				
			}
			jsonParser.close();
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//3. Pour the extracted information to a csv file
		//4. Clean the scene
	}
}
