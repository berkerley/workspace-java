package edu.tamu;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import org.apache.commons.lang3.StringEscapeUtils;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

public class Json2CSV_business {
	
	public static final String jsonFilePath = "E:\\data\\json\\yelp_academic_dataset_business_sample.json";
	public static final String csvFilePath = "E:\\data\\json\\yelp_academic_dataset_business_sample.csv";
	
	public static StringBuilder titleBuilder = new StringBuilder();
	public static String separator = "; ";
	
	public static void main(String[] args){
		
		File csvFile = new File(csvFilePath);
		FileWriter csvWriter = null;
		if(!csvFile.exists()){
			try {
				csvFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			csvWriter = new FileWriter(csvFile);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		//1. Prepare a json parser
		try {
			JsonFactory jsonFactory = new JsonFactory();
			JsonParser jsonParser = jsonFactory.createParser(new File(jsonFilePath));
			JsonToken token = jsonParser.nextToken();
			StringBuilder rowData = new StringBuilder();
			StringBuilder titleBuilder = new StringBuilder();
			int counter = 0;  //used to count # of tokens
			int rowCounter = 0;
			boolean buildTitle = true;
			while(token != null){
				String fieldName = jsonParser.getCurrentName();
				////System.out.println("--------------"+fieldName+"-----------------");
				//The next four string builders are temporarily used to hold the sub objects or arrays
				
				if("business id".equalsIgnoreCase(fieldName)){
					if(buildTitle)
						titleBuilder.append(wrapper(fieldName));
					
					counter++;
					////System.out.print(counter+". "+fieldName+"-->");
					token = jsonParser.nextToken();
					rowData.append(wrapper(jsonParser.getValueAsString()));  //"vcNAWiLM4d...."
					////System.out.println(jsonParser.getValueAsString());
					token = jsonParser.nextToken();
				}else if("full address".equalsIgnoreCase(fieldName) || "open".equalsIgnoreCase(fieldName) 
						|| "city".equalsIgnoreCase(fieldName) || "review count".equalsIgnoreCase(fieldName)
						|| "name".equalsIgnoreCase(fieldName) || "longitude".equalsIgnoreCase(fieldName)
						|| "state".equalsIgnoreCase(fieldName) || "stars".equalsIgnoreCase(fieldName)
						|| "latitude".equalsIgnoreCase(fieldName) || "type".equalsIgnoreCase(fieldName)){
					if(buildTitle){
						titleBuilder.append(separator);
						titleBuilder.append(wrapper(fieldName));
					}
					
					counter++;
					//System.out.print(counter+". "+fieldName+"-->");
					token = jsonParser.nextToken();
					rowData.append(separator);
					rowData.append(wrapper(jsonParser.getValueAsString()));
					//System.out.println(StringEscapeUtils.escapeJava(jsonParser.getValueAsString()));
					token = jsonParser.nextToken();
				}else if("categories".equalsIgnoreCase(fieldName) || "neighborhoods".equalsIgnoreCase(fieldName)){
					if(buildTitle){
						titleBuilder.append(separator);
						titleBuilder.append(wrapper(fieldName));
					}
					
					counter++;
					//System.out.print(counter+". "+fieldName+"-->");
					
					rowData.append(separator);
					
					StringBuilder arrayBuilder = new StringBuilder();
					while((token = jsonParser.nextToken()) != JsonToken.END_ARRAY){
						if(token == JsonToken.START_ARRAY){
							arrayBuilder.append(token.asString());  //starts with [
						}else{
							arrayBuilder.append(jsonParser.getValueAsString());
							arrayBuilder.append(", ");
						}
					}
					//The last separator is not removed
					if(token == JsonToken.END_ARRAY){
						arrayBuilder.append(token.asString());
					}
					String arrayString = arrayBuilder.toString();
					//System.out.println(StringEscapeUtils.escapeJava(arrayString));
					rowData.append(wrapper(arrayString));  // ]
					
					token = jsonParser.nextToken();
				}else if("hours".equalsIgnoreCase(fieldName)){
					if(buildTitle){
						titleBuilder.append(separator);
						titleBuilder.append(wrapper(fieldName));
					}
					
					counter++;
					//System.out.print(counter+". "+fieldName+"-->");
					
					rowData.append(separator);
					
					StringBuilder objectBuilder = new StringBuilder();
					JsonToken thisToken = jsonParser.nextToken();  //starts with {
					String thisName = thisToken.asString();
					
					////System.out.println(">>>>>>>"+thisName);
					
					JsonToken nextToken = jsonParser.nextToken();  //starts with FIELD_NAME or }
					String nextName = null;
					if(nextToken == JsonToken.END_OBJECT)
						nextName = nextToken.asString();
					else
						nextName = jsonParser.getValueAsString();  //can be field name or value
					////System.out.println(">>>>>>>"+nextName);
					
					while(!(thisToken == JsonToken.END_OBJECT 
							&& nextToken == JsonToken.FIELD_NAME 
							&& "open".equalsIgnoreCase(nextName))){
						
						
						objectBuilder.append(thisName).append("_");  
						//objectBuilder.append(nextName);
						thisToken = nextToken;  //shift by one
						if(thisToken == JsonToken.START_OBJECT || thisToken == JsonToken.END_OBJECT)
							thisName = thisToken.asString();
						else
							thisName = jsonParser.getValueAsString();
						
						nextToken = jsonParser.nextToken();  //shift by one
						if(nextToken == JsonToken.START_OBJECT || nextToken == JsonToken.END_OBJECT)
							nextName = nextToken.asString();
						else
							nextName = jsonParser.getValueAsString();
					}
					objectBuilder.append(thisName);
					String objectString = objectBuilder.toString();
					
					//System.out.println(StringEscapeUtils.escapeJava(objectString));
					rowData.append(wrapper(objectString));
					
					token = nextToken;
				}else if("attributes".equalsIgnoreCase(fieldName)){
					if(buildTitle){
						titleBuilder.append(separator);
						titleBuilder.append(wrapper(fieldName));
					}
					
					counter++;
					//System.out.print(counter+". "+fieldName+"-->");
					rowData.append(separator);
					
					StringBuilder attributesBuilder = new StringBuilder();
					
					while((token= jsonParser.nextToken()) != JsonToken.END_OBJECT){
						if(token == JsonToken.START_OBJECT)
							attributesBuilder.append(token.asString()).append("_");
						else
							attributesBuilder.append(jsonParser.getValueAsString()).append("_");
					}
					if(token == JsonToken.END_OBJECT)
						attributesBuilder.append(token.asString());
					String attributesString = attributesBuilder.toString();
					
					//System.out.println(attributesString);
					rowData.append(wrapper(attributesString));
					
					token = jsonParser.nextToken();
				}else{
					token = jsonParser.nextToken();
				}
				if(counter == 15){  //means one row is done
					//here you should create a csv file and pour the result row string to the csv file
					if(buildTitle){
						//System.out.println(titleBuilder.toString());
						csvWriter.write(titleBuilder.toString());
						csvWriter.write("\n");
						csvWriter.flush();
						buildTitle = false;
					}
					rowCounter++;
					System.out.println("row "+rowCounter+" data transformed to csv!");
					counter = 0;
					csvWriter.write(StringEscapeUtils.escapeJava(StringEscapeUtils.escapeJava(rowData.toString())));
					csvWriter.write("\n");
					csvWriter.flush();
					rowData.setLength(0);
					rowData = new StringBuilder();
				}
			}
			if(csvWriter != null)
				csvWriter.close();
			System.out.println(csvFilePath+" generated!");
		} catch (JsonParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	//This method is used to wrap the value with quote
	public static String wrapper(String content){
		StringBuilder wrapped = new StringBuilder();
		//return wrapped.append("'").append(content).append("'").toString();
		return content;
	}
}
