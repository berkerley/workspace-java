package edu.tamu;

public class StudentInfo implements Comparable{
	private String studentName;
	private String email;
	private String uin;
	
	public StudentInfo(){}
	public StudentInfo(String studentName, String email, String uin){
		this.studentName = studentName;
		this.email = email;
		this.uin = uin;
	}
	public String getStudentName() {
		return studentName;
	}
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getUin() {
		return uin;
	}
	public void setUin(String uin) {
		this.uin = uin;
	}
	@Override
	public int compareTo(Object o) {
		// TODO Auto-generated method stub
		return 0;
	}
	
}
