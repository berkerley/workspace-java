package edu.tamu;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;

public class GradeUpload {
	public static void main(String[] args) throws IOException {

		String readFilePath = "E:\\document\\professional\\Courses\\CSCE 221\\2015\\Programming Assignment\\PA 2\\PA2.xlsx";
		String readResultFilePath = "E:\\document\\professional\\Courses\\CSCE 221\\2015\\Programming Assignment\\PA 2\\1.csv";
		String writeResultFilePath = "E:\\document\\professional\\Courses\\CSCE 221\\2015\\Programming Assignment\\PA 2\\1-2.csv";
		// 1. Read xlsx file
		// 2. Re-organize the data, iterate the rows of the sheet and generate
		// the needed data format
		Sheet mySheet = ReadFile_XLSX(readFilePath);
		// 3. Write the assembled data format to the destination file
		mySheet = consolidate(mySheet);
		//writeFile_XLS(mySheet, readResultFilePath, writeResultFilePath);
		writeFile_CSV(mySheet, readResultFilePath, writeResultFilePath);
		System.out.println("Done!");
	}

	private static void writeFile_CSV(Sheet mySheet, String readResultFilePath, String writeResultFilePath) throws IOException {
		CSVReader reader = new CSVReader(new FileReader(readResultFilePath), ',' , '"' , 0);
		List<String[]> allRows = reader.readAll();
		for(String[] row : allRows){
			String uin = row[3];
			StudentRow studentRow = getStudentRow(uin, mySheet);
			if(studentRow != null){
				row[5] = studentRow.getTotal();
				row[8] = studentRow.getComment();
			}
		}
		reader.close();
		CSVWriter writer = new CSVWriter(new FileWriter(writeResultFilePath));
		writer.writeAll(allRows);
		writer.close();
	}

	private static Sheet consolidate(Sheet mySheet) {
		List<StudentRow> studentRowList = mySheet.getStudentRowList();
		int num = studentRowList.size();
		System.out.println("There are overall " + num + " student rows");
		for (int index = 0; index < num; index++) {
			StudentRow studentRow = studentRowList.get(index);
			StudentInfo info = studentRow.getStudentInfo();
			List<Answer> answerList = studentRow.getAnswerList();
			String total = studentRow.getTotal();
			
			StringBuilder commentBuilder = new StringBuilder();
			
			commentBuilder.append(displayStudentInfo(info));
			commentBuilder.append("\n");
			commentBuilder.append(displayAnswerList(answerList));
			commentBuilder.append("\n");
			commentBuilder.append("TOTAL----" + total);
			
			studentRow.setComment(commentBuilder.toString());
			studentRowList.set(index, studentRow);
		}
		mySheet.setStudentRowList(studentRowList);
		return mySheet;
	}

	private static void writeFile_XLS(Sheet mySheet, String readResultFilePath, String writeResultFilePath) throws IOException {
		
		InputStream ExcelFileToRead = new BufferedInputStream(new FileInputStream(readResultFilePath));
		XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);
		DataFormatter formatter = new DataFormatter();
		
		XSSFSheet sheet = wb.getSheetAt(0);
		XSSFRow row;

		Iterator<Row> rows = sheet.rowIterator();
		rows.next();
		while (rows.hasNext()) {
			row = (XSSFRow) rows.next();
			
			Cell cell3 = row.getCell(3);
			Cell cell5 = row.createCell(5);
			Cell cell8 = row.createCell(8);
			String uin = formatter.formatCellValue(cell3);
			StudentRow studentRow = getStudentRow(uin, mySheet);
			if(studentRow != null){
				cell5.setCellValue(studentRow.getTotal());
				cell8.setCellValue(studentRow.getComment());
			}
		}
		ExcelFileToRead.close();
		
		FileOutputStream fileOut = new FileOutputStream(writeResultFilePath);
		wb.write(fileOut);
		fileOut.flush();
		fileOut.close();
		wb.close();
	}

	private static StudentRow getStudentRow(String uin, Sheet mySheet) {
		StudentRow result = null;
		List<StudentRow> studentRowList = mySheet.getStudentRowList();
		int num = studentRowList.size();
		for(int index = 0; index < num; index++){
			StudentRow studentRow = studentRowList.get(index);
			String studentUin = studentRow.getStudentInfo().getUin();
			if(uin.trim().equalsIgnoreCase(studentUin.trim())){
				result = studentRow;
				break;
			}
		}
		return result;
	}

	private static String displayAnswerList(List<Answer> answerList) {
		StringBuilder answerListBuilder = new StringBuilder();
		int num = answerList.size();
		for (int index = 0; index < num; index++) {
			Answer answer = answerList.get(index);
			Question question = answer.getQuestion();
			answerListBuilder.append("-------------------" + question.getQuestionName() + "------------------\n");
			int itemNum = question.getItemNum();
			List<String> itemList = question.getItemList();
			List<String> sub_score_list = answer.getSub_score_list();
			for (int count = 0; count < itemNum; count++) {
				answerListBuilder.append(itemList.get(count) + "--> " + sub_score_list.get(count)+"\n");
			}
			answerListBuilder.append("\n");
		}
		return answerListBuilder.toString();
	}

	private static String displayStudentInfo(StudentInfo info) {
		StringBuilder infoBuilder = new StringBuilder();
		infoBuilder.append("NAME: " + info.getStudentName()+"\n");
		infoBuilder.append("EMAIL: " + info.getEmail()+"\n");
		infoBuilder.append("UIN: " + info.getUin()+"\n");
		return infoBuilder.toString();
	}

	public static Sheet ReadFile_XLSX(String readFilePath) throws IOException {
		InputStream ExcelFileToRead = new FileInputStream(readFilePath);
		XSSFWorkbook wb = new XSSFWorkbook(ExcelFileToRead);
		FormulaEvaluator evaluator = wb.getCreationHelper().createFormulaEvaluator();

		XSSFSheet sheet = wb.getSheetAt(0);
		Row row;
		Cell cell;
		Iterator<Row> rows = sheet.rowIterator();

		DataFormatter formatter = new DataFormatter();

		// Initialization
		Sheet mySheet = new Sheet();
		List<Question> questionList = mySheet.getQuestionList();
		List<StudentRow> studentRowList = mySheet.getStudentRowList();

		// Process the title row
		Row row_title = rows.next(); // row title
		Iterator<Cell> cells_title = row_title.cellIterator();

		String questionName = null;
		int itemNumList[] = { 6, 4, 3, 13 };
		int counter = 0;
		Question question = null;
		while (cells_title.hasNext()) {
			questionName = formatter.formatCellValue(cells_title.next());
			if (questionName != null && !"".equals(questionName.trim())
					&& !questionName.trim().toLowerCase().contains("total")) {
				question = new Question(questionName, itemNumList[counter++]);
				questionList.add(question);
			}
		}

		// Preprocess the items row
		Row row_items = rows.next();
		Iterator<Cell> cells_items = row_items.iterator();
		cells_items.next();
		cells_items.next();
		cells_items.next(); // bypass the first three items, NAME EMAIL UIN

		// fill in the itemList into each corresponding question
		int questionNum = questionList.size();
		for (int questionIndex = 0; questionIndex < questionNum; questionIndex++) {
			question = questionList.get(questionIndex);
			List<String> itemList = new ArrayList<String>();
			int num = question.getItemNum();
			for (int itemIndex = 0; itemIndex < num; itemIndex++) {
				itemList.add(formatter.formatCellValue(cells_items.next()));
			}
			question.setItemList(itemList);
			questionList.set(questionIndex, question);
		}
		/*
		 * for(int i = 0; i < questionList.size(); i++){ Question question_test
		 * = questionList.get(i);
		 * System.out.println(question_test.getQuestionName()); List<String>
		 * itemList = question_test.getItemList(); int num =
		 * question_test.getItemNum(); for(int itemIndex = 0; itemIndex < num;
		 * itemIndex++){ System.out.println(itemList.get(itemIndex)); }
		 * System.out.println(); }
		 */
		mySheet.setQuestionList(questionList);

		// The first two rows got processed. Now formally begin to process the
		// real data
		// Process the answer rows

		StudentInfo info = null;
		List<Answer> answerList = null;
		String total = null;

		StudentRow studentRow = null;

		while (rows.hasNext()) {
			row = rows.next();
			// System.out.println("Process row "+row.getRowNum());
			Iterator<Cell> cells = row.cellIterator();
			String name = formatter.formatCellValue(cells.next());
			if (name.trim().contains("221-50")) {
				continue;
			}
			if ("end".equals(name))
				break; // used to end the process.
			// Get the student info
			String email = formatter.formatCellValue(cells.next());
			String uin = formatter.formatCellValue(cells.next());

			info = new StudentInfo(name, email, uin);

			// Get the answer list
			answerList = new ArrayList<Answer>();
			Answer answer = null;
			String sub_score = null;
			for (int questionIndex = 0; questionIndex < questionNum; questionIndex++) {
				question = questionList.get(questionIndex);
				answer = new Answer(question);
				int num = question.getItemNum();
				List<String> sub_score_list = new ArrayList<String>(num);
				for (int itemIndex = 0; itemIndex < num; itemIndex++) {
					sub_score = formatter.formatCellValue(cells.next());
					 System.out.print(itemIndex+"-->"+sub_score+" ");
					sub_score_list.add(sub_score);
				}
				 System.out.println();
				answer.setSub_score_list(sub_score_list);
				answerList.add(answer);
			}

			// Get total
			cell = cells.next();
			if (Cell.CELL_TYPE_FORMULA == cell.getCellType()){
				total = evaluator.evaluate(cell).formatAsString();
				if(total == null || "".equals(total.trim())){
					//total
				}
			}else
				total = formatter.formatCellValue(cell);

			studentRow = new StudentRow(info, answerList, total);

			studentRowList.add(studentRow);
		}

		mySheet.setStudentRowList(studentRowList);
		wb.close();
		return mySheet;
	}

}
