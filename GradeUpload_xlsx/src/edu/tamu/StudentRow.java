package edu.tamu;

import java.util.ArrayList;
import java.util.List;

public class StudentRow {
	private StudentInfo info = new StudentInfo();
	private List<Answer> answerList = new ArrayList<Answer>();
	private String total;
	private String comment;
	
	public StudentRow(){}
	
	public StudentRow(StudentInfo info, List<Answer> answerList, String total) {
		super();
		this.info = info;
		this.answerList = answerList;
		this.total = total;
	}

	public StudentInfo getStudentInfo() {
		return info;
	}
	public void setStudentInfo(StudentInfo info) {
		this.info = info;
	}
	public List<Answer> getAnswerList() {
		return answerList;
	}
	public void setAnswerList(List<Answer> answerList) {
		this.answerList = answerList;
	}
	public String getTotal() {
		return total;
	}
	public void setTotal(String total) {
		this.total = total;
	}

	public StudentInfo getInfo() {
		return info;
	}

	public void setInfo(StudentInfo info) {
		this.info = info;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}
}
