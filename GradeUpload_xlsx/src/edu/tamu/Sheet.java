package edu.tamu;

import java.util.ArrayList;
import java.util.List;

public class Sheet {
	
	private List<Question> questionList = new ArrayList<Question>();
	private List<StudentRow> studentRowList = new ArrayList<StudentRow>();
	
	public List<Question> getQuestionList() {
		return questionList;
	}
	public void setQuestionList(List<Question> questionList) {
		this.questionList = questionList;
	}
	public List<StudentRow> getStudentRowList() {
		return studentRowList;
	}
	public void setStudentRowList(List<StudentRow> studentRowList) {
		this.studentRowList = studentRowList;
	}
	
}
