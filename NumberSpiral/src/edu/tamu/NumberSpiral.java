package edu.tamu;

public class NumberSpiral {
	public static void main(String[] args){
		int sum = 1;
		int last = 1;
		int index = 1;
		for(;index <= 1000;index++){
			last = last + index * 2;
			sum += 2 * last;
			if(index % 2 == 0){
				sum += index * 2;
			}
		}
		System.out.println("sum of 1001*1001 diagonals is "+ sum);
	}
}
