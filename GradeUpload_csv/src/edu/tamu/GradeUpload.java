package edu.tamu;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

/**
 * @author Benke Qu
 * @since 10/13/15
 * @version This version is an updated version which directly analyses csv file instead of excel file
 * Be careful:
 * 1. total column is the last column
 */



public class GradeUpload {
	public static String rootFilePath = "E:\\document\\professional\\courses\\csce221\\2015\\Programming Assignment\\PA 3\\upload";
	public static String googleGradeTablePath = rootFilePath + File.separator+ "google.csv";
	public static String ecampusFilePath_download = rootFilePath + File.separator+ "ecampus.csv";
	public static String ecampusFilePath_upload = rootFilePath + File.separator+ "ecampus-2.csv";
	public static final int STARTROW = 2;  //the starting row where the actual data is stored
	public static final int UINCOLUMN_GOOGLE = 2;  //starts from 0, the column which stores uin in the grade table
	public static final int TOTALCOLUMN_GOOGLE_REVERSE = 2;  //starts from 1, 0 means there is no total column
	public static final int UINCOLUMN_ECAMPUS = 3;  //starts from 0, the column which stores uin in the result file
	public static final int TOTALCOLUMN_ECAMPUS = 5;  //starts from 0, the column which stores total grade in the result file
	public static final int COMMENTCOLUMN_ECAMPUS = 8;  //starts from 0, the column which stores comments in the result file
	public static final String INITIALCOMMENT = "";
	
	public static void main(String[] args) throws IOException{
		//1. Read in all the data from the grade csv file
		CSVReader reader = new CSVReader(new FileReader(googleGradeTablePath), ',' , '"' , 0);
		List<String[]> allRows = reader.readAll();
		reader.close();
		if(allRows == null || allRows.isEmpty()) return;
		//2. Get the question list
		
		//2.1 Process the first row--title row
		String[] titleRow = allRows.get(0);
		
		List<Question> questionList = new ArrayList<Question>();
		List<String> questionNameList = new ArrayList<String>();
		List<Integer> questionItemNumList = new ArrayList<Integer>();
		String eachQuestionName = null;
		Question question = null;
		int preIndex = 0;
		int currentIndex = 1;  //skip the first cell which has the question name
		int preItemCount = 0;
		//Get the questionNameList and questionItemNumList and then the questionList
		for(; currentIndex < titleRow.length; currentIndex++){
			if(titleRow[currentIndex] != null && !"".equals(titleRow[currentIndex])){
				preItemCount = currentIndex - preIndex;
				questionNameList.add(titleRow[preIndex]);
				questionItemNumList.add(preItemCount);
				questionList.add(new Question(titleRow[preIndex], preItemCount));
				preIndex = currentIndex;
			}
		}
		//Add the last one question
		preItemCount = currentIndex - preIndex;  //the number of the last question
		questionNameList.add(titleRow[preIndex]);
		questionItemNumList.add(preItemCount);
		questionList.add(new Question(titleRow[preIndex], preItemCount));
		if(questionList.isEmpty()) return;
		//displayQuestionList(questionList);
		
		//2.2 Process the second row--item row
		//used to add itemlist for each question
		String[] itemRow = allRows.get(1);
		
		Question eachQuestion = null;
		int itemNumEachQuestion = 0;
		List<String> itemList = new ArrayList<String>();
		int current = 0;
		for(int index = 0; index < questionList.size(); index++){
			eachQuestion = questionList.get(index);
			itemNumEachQuestion = eachQuestion.getItemNum();
			for(int columnCount = 0; columnCount < itemNumEachQuestion; columnCount++){
				itemList.add(itemRow[columnCount+current]);  //item can be null, like the last total column
			}
			current += itemNumEachQuestion;
			eachQuestion.setItemList(itemList);
			questionList.set(index, eachQuestion);
			itemList = new ArrayList<String>();
		}
		
		displayQuestionList(questionList);
		
		//3. Process the actual data
		//3.1 Read each row of data, assemble one comment and get the total grade
		CSVReader resultReader = new CSVReader(new FileReader(ecampusFilePath_download), ',' , '"' , 0);
		List<String[]> allEcampusRows = resultReader.readAll();
		resultReader.close();
		
		for(int rowCount = STARTROW; rowCount < allRows.size(); rowCount++){
			String[] currentRow = allRows.get(rowCount);
			StringBuilder commentBuilder = new StringBuilder(INITIALCOMMENT);
			String comment = null;
			String total = "0";
			int currentColumn = 0;
			Question question3 = null;
			int itemNum = 0;
			for(int index = 0; index < questionList.size(); index++){
				question3 = questionList.get(index);
				commentBuilder.append("\n");
				commentBuilder.append("----------").append(question3.getQuestionName()).append("----------\n");
				itemNum = question3.getItemNum();
				for(int columnCount = 0; columnCount < itemNum; columnCount++){
					commentBuilder.append(itemRow[columnCount+currentColumn]).append("-->");
					commentBuilder.append(currentRow[columnCount+currentColumn]).append("\n");
				}
				currentColumn += itemNum;
				if(index == questionList.size()-TOTALCOLUMN_GOOGLE_REVERSE){  //total column
					total = currentRow[currentColumn-itemNum];
				}
			}
			comment = commentBuilder.toString();
			
			for(String[] ecampusRow : allEcampusRows){
				if(ecampusRow[UINCOLUMN_ECAMPUS].trim().equals(currentRow[UINCOLUMN_GOOGLE].trim())){
					ecampusRow[TOTALCOLUMN_ECAMPUS] = total;
					System.out.println(comment);
					ecampusRow[COMMENTCOLUMN_ECAMPUS] = comment;
				}
			}
		}
		for(String[] ecampusRow : allEcampusRows){
			if(ecampusRow[TOTALCOLUMN_ECAMPUS] == null || "".equals(ecampusRow[TOTALCOLUMN_ECAMPUS])){
				ecampusRow[TOTALCOLUMN_ECAMPUS] = "0";
				ecampusRow[COMMENTCOLUMN_ECAMPUS] = "No submission";
			}
		}
		//write the result
		CSVWriter writer = new CSVWriter(new FileWriter(ecampusFilePath_upload));
		writer.writeAll(allEcampusRows);
		writer.close();
		System.out.println("Done!");
	}
	private static void displayQuestionList(List<Question> questionList) {
		for(Question question : questionList){
			System.out.println(question.getQuestionName()+"-------------");
			System.out.println(question.getItemNum()+" items:");
			List<String> itemList = question.getItemList();
			if(itemList != null && !itemList.isEmpty()){
				for(int index = 1; index <= itemList.size(); index++){
					System.out.println(itemList.get(index-1));
				}
			}
		}
	}
}