package edu.tamu;

import java.util.ArrayList;
import java.util.List;

public class Answer {
	Question question;
	List<String> sub_score_list;
	
	public Answer(){}
	
	public Answer(Question question){
		this.question = question;
		int itemNum = question.getItemNum();
		sub_score_list = new ArrayList<String>(itemNum);
	}

	public Question getQuestion() {
		return question;
	}

	public void setQuestion(Question question) {
		this.question = question;
	}

	public List<String> getSub_score_list() {
		return sub_score_list;
	}

	public void setSub_score_list(List<String> sub_score_list) {
		this.sub_score_list = sub_score_list;
	}
	
}
