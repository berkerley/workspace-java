package edu.tamu;


import java.util.ArrayList;
import java.util.List;

public class Question implements Comparable{
	private String questionName;
	private int itemNum;
	private List<String> itemList = new ArrayList<String>();
	
	public Question(String questionName){
		this.questionName = questionName;
	}
	
	public Question(String questionName, int itemNum){
		this.questionName = questionName;
		this.itemNum = itemNum;
	}
	
	public String getQuestionName() {
		return questionName;
	}
	public void setQuestionName(String questionName) {
		this.questionName = questionName;
	}
	public List<String> getItemList() {
		return itemList;
	}
	public void setItemList(List<String> itemList) {
		this.itemList = itemList;
	}
	@Override
	public int compareTo(Object arg0) {
		return 0;
	}
	public int getItemNum() {
		return itemNum;
	}
	public void setItemNum(int itemNum) {
		this.itemNum = itemNum;
	}
	
}
