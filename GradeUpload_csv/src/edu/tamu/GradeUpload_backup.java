package edu.tamu;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;
/**
 * @author Benke Qu
 * @since 10/13/15
 * @version This version is an updated version which directly analyses csv file instead of excel file
 * Be careful:
 * 1. total column is the last column
 */



public class GradeUpload_backup {
	public static String readGradeFilePath = "E:\\document\\professional\\Courses\\CSCE 221\\2015\\Homework\\HW1\\1.csv";
	public static String readResultFilePath = "E:\\document\\professional\\Courses\\CSCE 221\\2015\\Homework\\HW1\\2.csv";
	public static String writeResultFilePath = "E:\\document\\professional\\Courses\\CSCE 221\\2015\\Homework\\HW1\\2-2.csv";
	public static final int STARTROW = 2;  //the starting row where the actual data is stored
	public static final int UINCOLUMN_GRADE = 2;  //the column which stores uin in the grade table
	public static final int UINCOLUMN_RESULT = 3;  //the column which stores uin in the result file
	public static final int TOTALCOLUMN_RESULT = 5;  //the column which stores total grade in the result file
	public static final int COMMENTCOLUMN_RESULT = 8;  //the column which stores comments in the result file
	
	public static void main(String[] args) throws IOException{
		//1. Read in all the data from the grade csv file
		CSVReader reader = new CSVReader(new FileReader(readGradeFilePath), ',' , '"' , 0);
		List<String[]> allRows = reader.readAll();
		reader.close();
		if(allRows == null || allRows.isEmpty()) return;
		//2. Get the question list
		//2.1 Process the first row--title row
		int counter = 0;
		String questionName = "Student Info";
		String eachQuestionName = null;
		Question question = null;
		List<Question> questionList = new ArrayList<Question>();
		String[] titleRow = allRows.get(0);
		
		for(int index = 0; index < titleRow.length; index++){
			counter++;
			eachQuestionName = titleRow[index];
			if (eachQuestionName != null && !"".equals(eachQuestionName.trim())) {
				question = new Question(questionName, counter-1);
				questionList.add(question);
				counter = 1;
				questionName = eachQuestionName;
			}
		}
		//add the last total column
		question = new Question(questionName, counter);
		questionList.add(question);
		if(questionList.isEmpty()) return;
		
		//displayQuestionList(questionList);
		
		//2.2 Process the second row--item row
		Question question2 = null;
		int itemNumEachQuestion = 0;
		List<String> itemList = new ArrayList<String>();
		String[] itemRow = allRows.get(1);
		int current = 0;
		int[] itemNumList = new int[questionList.size()];
		for(int index = 0; index < questionList.size(); index++){
			question2 = questionList.get(index);
			itemNumEachQuestion = question2.getItemNum();
			itemNumList[index] = itemNumEachQuestion;
			itemList = question2.getItemList();
			for(int columnCount = 0; columnCount < itemNumEachQuestion; columnCount++){
				//System.out.println(itemRow[columnCount+current]);
				itemList.add(itemRow[columnCount+current]);
			}
			current += itemNumEachQuestion;
			question2.setItemList(itemList);
			questionList.set(index, question2);
		}
		
		//displayQuestionList(questionList);
		
		//3. Process the actual data
		//3.1 Read each row of data, assemble one comment and get the total grade
		CSVReader resultReader = new CSVReader(new FileReader(readResultFilePath), ',' , '"' , 0);  //The final csv file uploaded to ecampus
		List<String[]> allResultRows = resultReader.readAll();
		resultReader.close();
		
		for(int rowCount = STARTROW; rowCount < allRows.size(); rowCount++){
			String[] currentRow = allRows.get(rowCount);
			StringBuilder commentBuilder = new StringBuilder();
			String comment = null;
			String total = null;
			int currentColumn = 0;
			Question question3 = null;
			int itemNum = 0;
			for(int index = 0; index < questionList.size(); index++){
				question3 = questionList.get(index);
				commentBuilder.append("\n");
				commentBuilder.append("----------").append(question3.getQuestionName()).append("----------\n");
				itemNum = question3.getItemNum();
				for(int columnCount = 0; columnCount < itemNum; columnCount++){
					commentBuilder.append(itemRow[columnCount+currentColumn]).append("-->");
					commentBuilder.append(currentRow[columnCount+currentColumn]).append("\n");
				}
				currentColumn += itemNum;
				if(index == questionList.size()-1){  //total column
					total = currentRow[currentColumn-1];
				}
			}
			comment = commentBuilder.toString();
			
			for(String[] resultRow : allResultRows){
				if(resultRow[UINCOLUMN_RESULT].trim().equals(currentRow[UINCOLUMN_GRADE].trim())){
					resultRow[TOTALCOLUMN_RESULT] = total;
					System.out.println(comment);
					resultRow[COMMENTCOLUMN_RESULT] = comment;
				}
			}
		}
		//write the result
		CSVWriter writer = new CSVWriter(new FileWriter(writeResultFilePath));
		writer.writeAll(allResultRows);
		writer.close();
		System.out.println("Done!");
	}
	private static void displayQuestionList(List<Question> questionList) {
		for(Question question : questionList){
			System.out.println(question.getQuestionName()+"-------------");
			System.out.println(question.getItemNum());
			List<String> itemList = question.getItemList();
			if(itemList != null && !itemList.isEmpty()){
				for(int index = 1; index <= itemList.size(); index++){
					System.out.println(itemList.get(index-1));
				}
			}
		}
	}
}























