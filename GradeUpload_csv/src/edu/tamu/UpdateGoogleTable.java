package edu.tamu;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.opencsv.CSVReader;
import com.opencsv.CSVWriter;

/**
 * @author Benke Qu
 * @since 10/22/15
 * @version This class is used to update the google spreadsheet
 * Be careful:
 * 1. total column is the last column
 * 2. One application is to upload the Lab Quiz #2 column
 */



public class UpdateGoogleTable {
	public static String sourceFilePath = "E:\\Professional\\Courses\\CSCE 221\\2015\\Programming Assignment\\PA 2\\quiz\\source.csv";
	public static String destFilePath_download = "E:\\Professional\\Courses\\CSCE 221\\2015\\Programming Assignment\\PA 2\\quiz\\destFilePath_download.csv";
	public static String destFilePath_upload = "E:\\Professional\\Courses\\CSCE 221\\2015\\Programming Assignment\\PA 2\\quiz\\destFilePath_upload.csv";
	public static final int STARTROW = 1;  //the starting row where the actual data is stored
	public static final int UINCOLUMN_GRADE = 1;  //the column which stores uin in the grade table
	public static final int UINCOLUMN_RESULT = 3;  //the column which stores uin in the result file
	public static final int TOTALCOLUMN_RESULT = 5;  //the column which stores total grade in the result file
	public static final int COMMENTCOLUMN_RESULT = 8;  //the column which stores comments in the result file
	
	public static void main(String[] args) throws IOException{
		//1. Read in all the data from the source csv file
		CSVReader reader = new CSVReader(new FileReader(sourceFilePath), ',' , '"' , 0);
		List<String[]> allSourceRows = reader.readAll();
		reader.close();
		if(allSourceRows == null || allSourceRows.isEmpty()) return;
		//2. Read in all the data from the destination csv file
		reader = new CSVReader(new FileReader(destFilePath_download), ',', '"', 0);
		List<String[]> allDestRows = reader.readAll();
		reader.close();
		if(allDestRows == null || allDestRows.isEmpty()) return;
		
		for(String[] destRow : allDestRows){
			if(destRow.length <= 1 || destRow[1] == null || "".equals(destRow[1].trim())){
				continue;
			}
			String uin = destRow[1].trim();
			destRow[3] = "0";
			for(String[] sourceRow : allSourceRows){
				if(sourceRow.length <= 1 || sourceRow[1] == null || "".equals(sourceRow[1].trim())){
					continue;
				}
				
				if(uin.equals(sourceRow[2].trim())){
					System.out.println("sourceRow[7] is" + sourceRow[7]);
					destRow[3] = sourceRow[7];
					break;
				}
			}
			//System.out.println(uin+" get grade "+destRow[3]);
		}
		CSVWriter writer = new CSVWriter(new FileWriter(destFilePath_upload));
		writer.writeAll(allDestRows);
		writer.close();
		System.out.println("Done!");
	}
}






























