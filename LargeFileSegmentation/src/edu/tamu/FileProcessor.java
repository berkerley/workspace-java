package edu.tamu;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.math.BigInteger;
import java.util.Scanner;

public class FileProcessor {
	//public static final String filePath = "F:\\Workspace\\Workspace-Java\\LargeFileSegmentation\\src\\edu\\tamu\\test\\test.txt";
	public static final String folderDirectory = "Z:\\Users\\William.Wang\\SO";
	//public static final String folderDirectory = "Z:\\Users\\William.Wang\\SO\\201208_stack_overflow_official\\home\\users\\ponza\\stack_overflow_201208";
	public static void main(String[] args) {
		//1. Environment setup (file/tools specifications)
		File parentFolder = null;
		//File childFolder = null;
		File subFile = null;
		String subFilePath = null;
		String subFileName = null;  //name of the subFile. Its style--> Post-298374928-1.txt
		FileInputStream fileInputStream = null;
		BufferedWriter fileWriter = null;
		Scanner fileScanner = null;
		String line = null;
		int counter = 0;  //This counter is used to count the number of file lines read in Posts.xml
		int order = 1;  //This order is used to work as a suffix of a file
		int numOfLines = 100000;
		
		try {
			//2. Scan the parent folder and get all the required folder path ready
			parentFolder = new File(folderDirectory);
			File[] childFolderList = parentFolder.listFiles(new FilenameFilterImp("stackoverflow.com", "startwith"));
			//File[] childFolderFileList = parentFolder.listFiles();
			File[] childFolderFileList = null;
			for(File childFolder : childFolderList){
				if(childFolder.isDirectory() && !childFolder.getName().contains("Badges")&& !childFolder.getName().contains("Users")&& !childFolder.getName().contains("Tags")){
					childFolderFileList = childFolder.listFiles(new FilenameFilterImp("xml", "endwith"));
					for(File childFolderFile : childFolderFileList){ 
						//3. Open the file located in the folder found under the child folder of the parent folder
						System.out.println("----------In "+childFolder.getName()+", going to read "+ childFolderFile.getName()+"----------");
						//System.out.println("----------In "+parentFolder.getName()+", going to read "+ childFolderFile.getName()+"----------");
						
						fileInputStream = new FileInputStream(childFolderFile);
						fileScanner = new Scanner(fileInputStream, "UTF-8");
						
						subFilePath = childFolderFile.getParent();
						subFileName = childFolderFile.getName().substring(0, childFolderFile.getName().lastIndexOf(".")); 
										//+ System.currentTimeMillis();
						subFilePath = subFilePath + File.separator + subFileName;
						System.out.println("subFilePath-->" + subFilePath);
						String tempSubFilePath = null;
						File tempSubFile = null;
						//fileWriter.append("----------In "+childFolder.getName()+", going to read "+ childFolderFile.getName()+"----------");
						//3. Read a large file by 40k lines and save as a sub txt file
						while(fileScanner.hasNextLine()){
							
							//3.1 Tell if current counter is 0. If so prepare the subFile and its fileWriter
							if(counter == 0){
								tempSubFilePath = subFilePath + "-" + order;
								tempSubFile = new File(tempSubFilePath);
								fileWriter = new BufferedWriter(new FileWriter(tempSubFile));
							}
							//3.2 filerWriter is appended with every line of inputs
							line = fileScanner.nextLine();
							//System.out.println(line);
							fileWriter.write(line);
							fileWriter.newLine();
							counter++;
							//3.3 Tell if current counter reaches 100,000. If so wrap up the subFile and close the fileWritr
							// and set counter == 0
							if(counter == numOfLines){
								System.out.println(tempSubFile.getName() + " gets generated.");
								System.out.println("Overall " + order * counter +" lines have been extracted into new sub files");
		
								fileWriter.close();
								counter = 0;
								order++;
							}
						}
						//3.4 If counter != 0. This means there still remains some lines to be written
						if(counter != 0){
							System.out.println(tempSubFile.getName() + " gets generated.");
							System.out.println("Overall " + (new BigInteger(String.valueOf(order))
																.multiply(new BigInteger(String.valueOf(numOfLines)))
																.add(new BigInteger(String.valueOf(counter))).toString()) 
														  +" lines have been extracted into new sub files");
							if(fileWriter != null)
								fileWriter.close();
							
						}
						//4. Reset all the indicators and tools like close the FileInputStream, fileScanner and FileWriter
						counter = 0;
						order = 1;
						if(fileScanner.ioException() != null){
							throw fileScanner.ioException();
						}
						if (fileInputStream != null){
							try {
								fileInputStream.close();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
						}
						if(fileScanner != null){
							fileScanner.close();
						}
						if(fileWriter != null){
							fileWriter.close();
						}
					}
				}
			}
			if(fileWriter != null){
				fileWriter.close();
			}
			System.out.println("read done");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		} finally{
			if (fileInputStream != null){
				try {
					fileInputStream.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			if(fileScanner != null){
				fileScanner.close();
			}
		}
	}
}
/**
 * This class is used to single out those files that conform to the rules
 * @author Benke
 *
 */
class FilenameFilterImp implements FilenameFilter{
	private String filterRule = "";
	private String filterMethod = ""; 
	public FilenameFilterImp(String filterRule, String filterMethod){
		this.filterRule = filterRule;
		this.filterMethod = filterMethod;
	}
	@Override
	public boolean accept(File dir, String name) {
		String filterRule = getFilterRule();
		String filterMethod = getFilterMethod();
		if("startwith".equalsIgnoreCase(filterMethod))
			return name.startsWith(filterRule);
		if("endwith".equalsIgnoreCase(filterMethod))
			return name.endsWith(filterRule);
		return true;
	}
	public String getFilterRule() {
		return filterRule;
	}
	public void setFilterRule(String filterRule) {
		this.filterRule = filterRule;
	}
	public String getFilterMethod() {
		return filterMethod;
	}
	public void setFilterMethod(String filterMethod) {
		this.filterMethod = filterMethod;
	}
	
}
