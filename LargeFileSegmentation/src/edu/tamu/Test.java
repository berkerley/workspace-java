package edu.tamu;

import java.io.File;

public class Test {
	public static void main(String[] args){
		File test = new File("E:\\Non-professional\\logFile.txt");
		System.out.println("test path-->"+test.getParent());
		System.out.println("test path-->"+test.getAbsolutePath());
		System.out.println("test file name-->"+test.getName());
		
		int matrix[][] = {{1, 4, 7, 10}, {2, 5, 8, 11}, {3, 6, 9, 12}};
		for(int row = 0; row < matrix.length; row++){
			for(int index = 0; index < matrix[0].length; index++){
				System.out.print(matrix[row][index]+"\t");
			}
			System.out.println();
		}
		int vector[] = new int[matrix.length];  //This vector is used to store the min distance
		//It is a 3-fold for loop
		for(int row = 0; row < matrix.length; row++){
			int min = Integer.MAX_VALUE;
			int minRow = 0;
			for(int row_left = 0; row_left < matrix.length; row_left++){
				int quadraticSum = 0;
				if(row_left != row){  //only row_left is not equal to row, we do the calculation
					//calculate the quadratic sum of row and row_left
					for(int index = 0; index < matrix[0].length; index++){
						quadraticSum = (int) (quadraticSum + Math.pow((matrix[row][index] - matrix[row_left][index]), 2));
					}
					System.out.println("The quadractic sum of row "+row+" and row "+row_left+" is "+quadraticSum);
					//Tell if the quadratic sum is smaller than the current min
					if(quadraticSum < min){
						min = quadraticSum;
						minRow = row_left;
					}
				}
			}
			System.out.println("The min distance to row "+row+" is row "+minRow);
			vector[row] = minRow;
		}
	}
}
